FROM ubuntu:16.04

# update apt cache
RUN apt-get update

# install dependencies
RUN apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# download docker keys
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# add docker source
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Reupdate apt-get cache
RUN apt-get update

# install docker
RUN apt-get install -y docker-ce